#ifndef COMMON_H
#define COMMON_H

#include <e32base.h>
#include <e32cons.h>
#include <e32std.h>

#include "logger.h"

// The main function that will be called once the general setup is done.
TInt Main();

// The global logger handle. Allows us to abstract some things away into this header.
Logger* logger;

// Gets values of most registers without clobbering them.
inline void get_registers(TUint (&registers)[16])
{
	asm("MOV %0, PC\n" : "=r" (registers[15]));
	asm("MOV %0, SP\n" : "=r" (registers[13]));
	asm("MOV %0, LR\n" : "=r" (registers[14]));
	asm("MOV %0, R0\n" : "=r" (registers[0]));
	asm("MOV %0, R1\n" : "=r" (registers[1]));
	asm("MOV %0, R2\n" : "=r" (registers[2]));
	asm("MOV %0, R3\n" : "=r" (registers[3]));
	asm("MOV %0, R4\n" : "=r" (registers[4]));
	asm("MOV %0, R5\n" : "=r" (registers[5]));
	asm("MOV %0, R6\n" : "=r" (registers[6]));
	asm("MOV %0, R7\n" : "=r" (registers[7]));
	asm("MOV %0, R8\n" : "=r" (registers[8]));
	asm("MOV %0, R9\n" : "=r" (registers[9]));
	asm("MOV %0, R10\n" : "=r" (registers[10]));
	asm("MOV %0, R11\n" : "=r" (registers[11]));
	asm("MOV %0, R12\n" : "=r" (registers[12]));
}

// Prints the given registers, except CPSR.
inline void print_registers(TUint (&registers)[16])
{
	_LIT(KRegister, "R%d:0x%X\n");
	_LIT(KSP, "SP:0x%X\n");
	_LIT(KLR, "LR:0x%X\n");
	_LIT(KPC, "PC:0x%X\n");

	for (TUint i = 0; i < 13; ++i)
	{
		logger->log(KRegister, i, registers[i]);
	}

	logger->log(KSP, registers[13]);
	logger->log(KLR, registers[14]);
	logger->log(KPC, registers[15]);
}

// Prints the contents of all the registers for the current thread.
inline void print_registers()
{
	TUint registers[16];
	get_registers(registers);
	print_registers(registers);

	// TODO: How to get CPSR correctly?
	TUint CPSR;

	RDebug::Open();
	RDebug::GetRegister(RThread().Id(), 16, CPSR);
	RDebug::Close();

	_LIT(KCPSR, "CPSR:0x%X\n");
	logger->log(KCPSR, CPSR);
}

// The default exception handler.
void exception_handler(TExcType type)
{
	// We obtain the register values right at the beginning and print them later to avoid tampering with them.
	TUint registers[16];
	get_registers(registers);

	if (type == EExcAccessViolation)
	{
		_LIT(KAccessViolation, "Access violation!\n");
		logger->log(KAccessViolation);
	}
	else if (type == EExcPageFault)
	{
		_LIT(KPageFault, "Page fault!\n");
		logger->log(KPageFault);
	}
	else if (type == EExcInvalidOpCode)
	{
		_LIT(KInvalidOpcode, "Invalid opcode!\n");
		logger->log(KInvalidOpcode);
	}
	else
	{
		_LIT(KException, "Exception! %d\n");
		logger->log(KException, type);
	}

	print_registers();
	logger->get_char();
	User::Exit(type);
}

TUint32 lowest_address;
TUint32 highest_address;

void try_find_page_size_exception_handler(TExcType type)
{
	// Restore the default exception handler.
	RThread().SetExceptionHandler(&exception_handler, KExceptionAbort | KExceptionKill | KExceptionUserInterrupt | KExceptionFpe | KExceptionFault | KExceptionInteger);

	// Leave back to the user code with the found highest address.
	User::Leave(highest_address - lowest_address);
}

// Tries to find the size of a page by accessing every byte starting
// from the given address.
// The resulting highest accessible address is returned as the error code
// from a leave. One must trap this function to continue execution after
// it completes.
void try_find_page_size(TUint32 address)
{
	RThread().SetExceptionHandler(&try_find_page_size_exception_handler, KExceptionFault);

	lowest_address = address;
	highest_address = address;

	while (true)
	{
		volatile TUint8 lol = *reinterpret_cast<TUint8*>(highest_address++);
	}
}

TInt E32Main()
{
	// We need to create a cleanup stack, so we can use the cleanup stack and traps.
	CTrapCleanup* cleanup_stack = CTrapCleanup::New();

	// Set a default exception handler that prints debug info upon an exception
	RThread().SetExceptionHandler(&exception_handler, KExceptionAbort | KExceptionKill | KExceptionUserInterrupt | KExceptionFpe | KExceptionFault | KExceptionInteger);

	// Run the main function. Also obtain the return code or the error code in case of a leave.
	TInt result = 0;
	TRAPD(error, result = Main());

	// Finally delete the cleanup stack before returning.
	delete cleanup_stack;

	// If we trapped a leave, then we return that.
	if (error)
	{
		return error;
	}

	// Otherwise if the execution didn't leave, return the application return code.
	return result;
}

#endif